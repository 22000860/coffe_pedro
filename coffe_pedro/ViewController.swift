//
//  ViewController.swift
//  coffe_pedro
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Coffe: Decodable {
    let image: String
    let status: String
}

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNewCoffe()
    }

    @IBAction func reloadImage(_ sender: Any) {
        getNewCoffe()
    }
    
    func getNewCoffe() {
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Coffe.self){
            response in
            if let coffe = response.value {
                self.imageView.kf.setImage(with: URL(string: coffe.image))
            }
        }
    }
    
}
